﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Reflection;
using JobMarket.Repositories;
using JobMarket.Models;
using JobMarket.Extensions;

namespace JobMarket.Controllers
{
    public class JobsController : Controller
    {
        private readonly IJobsRepository jobRepository;

        public JobsController(IJobsRepository jobRepository)
        {
            this.jobRepository = jobRepository;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page, int rp, string sortname, string sortorder, string qtype, string query)
        {
            var jobs = this.jobRepository.GetAll().AsQueryable();

            if (!string.IsNullOrEmpty(qtype) && !string.IsNullOrEmpty(query))
            {
                jobs = jobs.Where(qtype, query);
            }

            if (!string.IsNullOrEmpty(sortname) && !string.IsNullOrEmpty(sortorder))
            {
                jobs = jobs.OrderBy(sortname, sortorder == "asc");
            }

            jobs = jobs.Skip((page - 1) * rp).Take(rp);

            int total = jobs.Count();

            return CreateFlexiJson(jobs.ToList(), page, total); 
        }

        [HttpPost]
        public ActionResult Save(JobPost jobPost)
        {
            this.jobRepository.Save(jobPost);

            var jobs = this.jobRepository.GetAll().OrderByDescending(x => x.ModifiedDate).AsQueryable();

            return CreateFlexiJson(jobs.ToList(), 1, jobs.Count()); 
        }

        [HttpPost]
        public ActionResult Delete(Guid[] ids)
        {
            foreach (var id in ids)
            {
                this.jobRepository.Delete(id);
            }

            var jobs = this.jobRepository.GetAll().OrderByDescending(x => x.ModifiedDate).AsQueryable();

            return CreateFlexiJson(jobs.ToList(), 1, jobs.Count());
        }

        private JsonResult CreateFlexiJson(IEnumerable<JobPost> items, int page, int total)
        {
            return Json(
                    new
                    {
                        page,
                        total,
                        rows =
                            items
                            .Select(x =>
                                new
                                {
                                    id = x.Id,
                                    // either use GetPropertyList(x) method to get all columns 
                                    cell = new object[] { x.Role, x.Description, x.JobType, x.Location }
                                })
                    }, JsonRequestBehavior.AllowGet);
        }

        /*private List<string> GetPropertyList(object obj)
        {
            var type = obj.GetType();
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

            return properties.Select(property => property.GetValue(obj, null)).Select(o => o == null ? "" : o.ToString()).ToList();
        }*/
    }
}
