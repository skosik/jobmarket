﻿using System.Web.Mvc;
using JobMarket.Repositories;
using StructureMap;

namespace JobMarket.IoC
{
    public class DependencyResolverInitializer
    {
        public static void Initialize() {
            Container container = new Container(x => {
                x.For(typeof(IJobsRepository)).Use(typeof(JobsRepository));
            });

            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
        }
    }
}